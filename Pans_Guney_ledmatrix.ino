/*
 * Auteur 1: Jethro Pans
 * Auteur 2: Oghuzan Guney
 * https://gitlab.com/cp2_arduino_8x8ledmatrix/cp2_arduino_8x8ledmatrix
 */

#include <TimerOne.h>

int st = 9;   //Latchpin
int sh = 11;  //Clockpin
int ds = 10;  //Datapin

int pot = A1; //2 potentiometers op PCB
int pot2 = A2;

int value;
int val;
unsigned long int snelheid = 100000;

int state = LOW;
volatile unsigned long count = 0;

void writeMatrix(uint16_t waarde);  //Aanmaken functies op uit te schrijven
int encodeMatrix(int matrix);

void setup() {
  pinMode(st, OUTPUT);
  pinMode(sh, OUTPUT);
  pinMode(ds, OUTPUT);

  pinMode(pot, INPUT);
  pinMode(pot2, INPUT);

  Serial.begin(9600);
}

int rollingEye(int count);
int sig(int count);
int CP(int count);
int smile(int count);

void loop() {
value = analogRead(pot);  //Potentiometer wordt uitgelezen.
if (value <= 100){        //Via deze if functie wordt aan de waarde van de ingelezen waarde een snelheid toegewezen in microseconden.
  snelheid = 50000;
  } else if (value <= 200){
    snelheid = 100000;
    } else if (value <= 300){
    snelheid = 170000;
    } else if (value <= 400){
    snelheid = 230000;
    } else if (value <= 500){
    snelheid = 300000;
    } else if (value <= 600){
    snelheid = 350000;
    } else if (value <= 700){
    snelheid = 400000;
    } else if (value <= 800){
    snelheid = 430000;
    } else if (value <= 900){
    snelheid = 470000;
    } else if (value > 900){
    snelheid = 500000;
    }
  
Timer1.initialize(snelheid);  //De toegewezen snelheid wordt ingesteld op de timer.
  Timer1.attachInterrupt(tim); //Telkens de timer optelt, wordt er een interrupt naar de functie tim() gestuurd. Deze geeft een getal door.
  
  unsigned long copy;

  noInterrupts();
  copy = count;
  interrupts();

val = analogRead(pot2); //Bij de tweede potentiometer wordt ook de waarde ingelezen. Hierbij kan er gewisseld worden van animatie bij bepaalde waardes. Telkens wordt er een animatie opgeroepen als functie. De count geeft door welk frame er wordt getoond.
Serial.println(val);
  if (val <= 250){
  rollingEye(count);
    }else if (val <= 500){
      sig(count);
   }else if (val <= 750){
    CP(count);
   }else if (val > 750){
    smile(count);
   }
  }

int rollingEye(volatile unsigned long count){
  switch(count){
     case 0 : writeMatrix(encodeMatrix(0xc381));
     writeMatrix(encodeMatrix(0x8142));
     writeMatrix(encodeMatrix(0x0024));
     writeMatrix(encodeMatrix(0x3018));
     break;
     case 1 : writeMatrix(encodeMatrix(0xc381));
     writeMatrix(encodeMatrix(0x8142));
     writeMatrix(encodeMatrix(0x0024));
     writeMatrix(encodeMatrix(0x6018));
     break;
     case 2 : writeMatrix(encodeMatrix(0xc381));
     writeMatrix(encodeMatrix(0x8142));
     writeMatrix(encodeMatrix(0x300c));
     writeMatrix(encodeMatrix(0x0030));
     break;
     case 3 : writeMatrix(encodeMatrix(0xc381));
     writeMatrix(encodeMatrix(0x9902));
     writeMatrix(encodeMatrix(0x1804));
     writeMatrix(encodeMatrix(0x0038));
     writeMatrix(encodeMatrix(0x8140));
     break;
     case 4 : writeMatrix(encodeMatrix(0xc381));
     writeMatrix(encodeMatrix(0x8142));
     writeMatrix(encodeMatrix(0x0c0c));
     writeMatrix(encodeMatrix(0x0030));
     break;
     case 5 : writeMatrix(encodeMatrix(0xc381));
     writeMatrix(encodeMatrix(0x8142));
     writeMatrix(encodeMatrix(0x0024));
     writeMatrix(encodeMatrix(0x0618));
     break;
     case 6 : writeMatrix(encodeMatrix(0xc381));
     writeMatrix(encodeMatrix(0x8142));
     writeMatrix(encodeMatrix(0x000c));
     writeMatrix(encodeMatrix(0x0c30));
     break;
     case 7 : writeMatrix(encodeMatrix(0xc381));
     writeMatrix(encodeMatrix(0x8102));
     writeMatrix(encodeMatrix(0x001c));
     writeMatrix(encodeMatrix(0x1820));
     writeMatrix(encodeMatrix(0x9940));
     break;
     case 8 : writeMatrix(encodeMatrix(0xc381));
     writeMatrix(encodeMatrix(0x8142));
     writeMatrix(encodeMatrix(0x000c));
     writeMatrix(encodeMatrix(0x3030));
     break;
     case 9 : writeMatrix(encodeMatrix(0xc381));
     writeMatrix(encodeMatrix(0x8142));
     writeMatrix(encodeMatrix(0x0024));
     writeMatrix(encodeMatrix(0x6018));
     break;
     case 10 : writeMatrix(encodeMatrix(0xc381));
     writeMatrix(encodeMatrix(0x8142));
     writeMatrix(encodeMatrix(0x0024));
     writeMatrix(encodeMatrix(0x3018));
     break;
     case 11 : writeMatrix(encodeMatrix(0xc381));
     writeMatrix(encodeMatrix(0x8142));
     writeMatrix(encodeMatrix(0x0024));
     writeMatrix(encodeMatrix(0x1818));
     break;
    }
  }

   int sig(volatile unsigned long count){
    switch(count){
     case 0:
     writeMatrix(encodeMatrix(0xdb80));
     writeMatrix(encodeMatrix(0xe740));
     writeMatrix(encodeMatrix(0xe720));
     writeMatrix(encodeMatrix(0xdb10));
     writeMatrix(encodeMatrix(0xbd0c));
     writeMatrix(encodeMatrix(0x7e02));
     writeMatrix(encodeMatrix(0x7e01));
     break;
     case 1:
     writeMatrix(encodeMatrix(0xdb80));
     writeMatrix(encodeMatrix(0xe740));
     writeMatrix(encodeMatrix(0xe720));
     writeMatrix(encodeMatrix(0xdb10));
     writeMatrix(encodeMatrix(0xbd0c));
     writeMatrix(encodeMatrix(0x7e02));
     writeMatrix(encodeMatrix(0x7e01));
     break;
     case 2:
     writeMatrix(encodeMatrix(0xe780));
     writeMatrix(encodeMatrix(0xe740));
     writeMatrix(encodeMatrix(0xdb20));
     writeMatrix(encodeMatrix(0xbd18));
     writeMatrix(encodeMatrix(0x7e04));
     writeMatrix(encodeMatrix(0x7e03));
     break;
     case 3:
     writeMatrix(encodeMatrix(0xe780));
     writeMatrix(encodeMatrix(0xdb40));
     writeMatrix(encodeMatrix(0xbd30));
     writeMatrix(encodeMatrix(0x7e08));
     writeMatrix(encodeMatrix(0x7e06));
     writeMatrix(encodeMatrix(0xbd01));
     break;
     case 4:
     writeMatrix(encodeMatrix(0xdb80));
     writeMatrix(encodeMatrix(0xbd60));
     writeMatrix(encodeMatrix(0x7e10));
     writeMatrix(encodeMatrix(0x7e0c));
     writeMatrix(encodeMatrix(0xbd03));
     break;
     case 5:
     writeMatrix(encodeMatrix(0xbdc0));
     writeMatrix(encodeMatrix(0x7e20));
     writeMatrix(encodeMatrix(0x7e18));
     writeMatrix(encodeMatrix(0xbd06));
     writeMatrix(encodeMatrix(0xdb01));
     break;  
     case 6:
     writeMatrix(encodeMatrix(0xbd80));
     writeMatrix(encodeMatrix(0x7e40));
     writeMatrix(encodeMatrix(0x7e30));
     writeMatrix(encodeMatrix(0xbd0c));
     writeMatrix(encodeMatrix(0xdb02));
     writeMatrix(encodeMatrix(0xe701));
     break;  
     case 7:
     writeMatrix(encodeMatrix(0x7e80));
     writeMatrix(encodeMatrix(0x7e60));
     writeMatrix(encodeMatrix(0xbd18));
     writeMatrix(encodeMatrix(0xdb04));
     writeMatrix(encodeMatrix(0xe702));
     writeMatrix(encodeMatrix(0xe701));
     break;  
     case 8:
     writeMatrix(encodeMatrix(0x7ec0));
     writeMatrix(encodeMatrix(0xbd30));
     writeMatrix(encodeMatrix(0xdb08));
     writeMatrix(encodeMatrix(0xe704));
     writeMatrix(encodeMatrix(0xe702));
     writeMatrix(encodeMatrix(0xdb01));
     break;  
     case 9:
     writeMatrix(encodeMatrix(0x7e80));
     writeMatrix(encodeMatrix(0xbd60));
     writeMatrix(encodeMatrix(0xdb10));
     writeMatrix(encodeMatrix(0xe708));
     writeMatrix(encodeMatrix(0xe704));
     writeMatrix(encodeMatrix(0xdb02));
     writeMatrix(encodeMatrix(0xbd01));
     break;  
     case 10:
     writeMatrix(encodeMatrix(0xbdc0));
     writeMatrix(encodeMatrix(0xdb20));
     writeMatrix(encodeMatrix(0xe710));
     writeMatrix(encodeMatrix(0xe708));
     writeMatrix(encodeMatrix(0xdb04));
     writeMatrix(encodeMatrix(0xbd03));
     break;  
     case 11:
     writeMatrix(encodeMatrix(0xbd80));
     writeMatrix(encodeMatrix(0xdb40));
     writeMatrix(encodeMatrix(0xe720));
     writeMatrix(encodeMatrix(0xe710));
     writeMatrix(encodeMatrix(0xdb08));
     writeMatrix(encodeMatrix(0xbd06));
     writeMatrix(encodeMatrix(0x7e01));
     break;  
    }
  }

  int CP(volatile unsigned long count){
  switch(count){
    case 0: 
    writeMatrix(encodeMatrix(0xc301));
    break;
    case 1: 
    writeMatrix(encodeMatrix(0xc302));
    writeMatrix(encodeMatrix(0xbd01));
    break;
    case 2: 
    writeMatrix(encodeMatrix(0xc304));
    writeMatrix(encodeMatrix(0xbd02));
    writeMatrix(encodeMatrix(0x7e01));
    break;
    case 3: 
    writeMatrix(encodeMatrix(0xc308));
    writeMatrix(encodeMatrix(0xbd04));
    writeMatrix(encodeMatrix(0x7e03));
    break;
    case 4: 
    writeMatrix(encodeMatrix(0xc310));
    writeMatrix(encodeMatrix(0xbd08));
    writeMatrix(encodeMatrix(0x7e07));
    break;
    case 5: 
    writeMatrix(encodeMatrix(0xc320));
    writeMatrix(encodeMatrix(0xbd10));
    writeMatrix(encodeMatrix(0x7e0e));
    break;
    case 6: 
    writeMatrix(encodeMatrix(0xc340));
    writeMatrix(encodeMatrix(0xbd20));
    writeMatrix(encodeMatrix(0x7e1c));
    writeMatrix(encodeMatrix(0x0001));
    break;
    case 7: 
    writeMatrix(encodeMatrix(0xc380));
    writeMatrix(encodeMatrix(0xbd40));
    writeMatrix(encodeMatrix(0x7e38));
    writeMatrix(encodeMatrix(0x0002));
    writeMatrix(encodeMatrix(0x6f01));
    break;
    case 8: 
    writeMatrix(encodeMatrix(0xbd80));
    writeMatrix(encodeMatrix(0x7e70));
    writeMatrix(encodeMatrix(0x0004));
    writeMatrix(encodeMatrix(0x6f03));
    break;
    case 9: 
    writeMatrix(encodeMatrix(0x7ee0));
    writeMatrix(encodeMatrix(0x0008));
    writeMatrix(encodeMatrix(0x6f06));
    writeMatrix(encodeMatrix(0x9f01));
    break;
    case 10: 
    writeMatrix(encodeMatrix(0x7ec0));
    writeMatrix(encodeMatrix(0x0010));
    writeMatrix(encodeMatrix(0x6f0c));
    writeMatrix(encodeMatrix(0x9f02));
    break;
    case 11: 
    writeMatrix(encodeMatrix(0x7e80));
    writeMatrix(encodeMatrix(0x0020));
    writeMatrix(encodeMatrix(0x6f18));
    writeMatrix(encodeMatrix(0x9f04));
    break;
    }
  }

int smile(volatile unsigned long count){
  switch(count){
    case 0: 
    writeMatrix(encodeMatrix(0xc381));
    writeMatrix(encodeMatrix(0xbd42));
    writeMatrix(encodeMatrix(0x5624));
    writeMatrix(encodeMatrix(0x7a18));
    break;
    case 1: 
    writeMatrix(encodeMatrix(0xc381));
    writeMatrix(encodeMatrix(0xbd42));
    writeMatrix(encodeMatrix(0x5a24));
    writeMatrix(encodeMatrix(0x7a18));
    break;
    case 2: 
    writeMatrix(encodeMatrix(0xc381));
    writeMatrix(encodeMatrix(0xbd42));
    writeMatrix(encodeMatrix(0x5a20));
    writeMatrix(encodeMatrix(0x4a04));
    writeMatrix(encodeMatrix(0x7618));
    break;
    case 3: 
    writeMatrix(encodeMatrix(0xc381));
    writeMatrix(encodeMatrix(0xbd42));
    writeMatrix(encodeMatrix(0x5a20));
    writeMatrix(encodeMatrix(0x5204));
    writeMatrix(encodeMatrix(0x7618));
    break;
    case 4: 
    writeMatrix(encodeMatrix(0xc381));
    writeMatrix(encodeMatrix(0xbd40));
    writeMatrix(encodeMatrix(0xb902));
    writeMatrix(encodeMatrix(0x5a24));
    writeMatrix(encodeMatrix(0x7618));
    break;
    case 5: 
    writeMatrix(encodeMatrix(0xc381));
    writeMatrix(encodeMatrix(0xbd40));
    writeMatrix(encodeMatrix(0xbc02));
    writeMatrix(encodeMatrix(0x5a24));
    writeMatrix(encodeMatrix(0x7618));
    break;
    case 6: 
    writeMatrix(encodeMatrix(0xc381));
    writeMatrix(encodeMatrix(0xbd42));
    writeMatrix(encodeMatrix(0x5a24));
    writeMatrix(encodeMatrix(0x7a18));
    break;
    case 7: 
    writeMatrix(encodeMatrix(0xc381));
    writeMatrix(encodeMatrix(0xbd42));
    writeMatrix(encodeMatrix(0x5a24));
    writeMatrix(encodeMatrix(0x7a18));
    break;
    case 8: 
    writeMatrix(encodeMatrix(0xc381));
    writeMatrix(encodeMatrix(0xbd42));
    writeMatrix(encodeMatrix(0x5a24));
    writeMatrix(encodeMatrix(0x7a18));
    break;
    case 9: 
    writeMatrix(encodeMatrix(0xc381));
    writeMatrix(encodeMatrix(0xbd42));
    writeMatrix(encodeMatrix(0x5624));
    writeMatrix(encodeMatrix(0x7a18));
    break;
    case 10: 
    writeMatrix(encodeMatrix(0xc381));
    writeMatrix(encodeMatrix(0xbd42));
    writeMatrix(encodeMatrix(0x5624));
    writeMatrix(encodeMatrix(0x7a18));
    break;
    case 11: 
    writeMatrix(encodeMatrix(0xc381));
    writeMatrix(encodeMatrix(0xbd42));
    writeMatrix(encodeMatrix(0x5624));
    writeMatrix(encodeMatrix(0x7a18));
    break;
  }
  }

void tim(void)        //Telkens er een signaal wordt doorgegeven, wordt count +1 gedaan. Het huidig getal wordt dan teruggestuurd. Wanneer de counter 21 bereikt, wordt deze terug op 0 gezet.
{
  if (state == LOW) {
    state = HIGH;
    count++;
    if (count == 12) {
      count = 0;
      }
  } else {
    state = LOW;
  }
}

    void writeMatrix(uint16_t waarde) {   //De omgevormde waarde wordt uitgeschreven op de led matrix d.m.v shift registers. De latchpin staat laag. Eerst worden de waardes om beurt gestuurd, telkens de clockpin hoog is. 
                                          //Daarna wordt de latchpin hoog gezet, als teken dat de reeks gedaan is en de volgende keer nieuwe waarden worden geschreven.
  digitalWrite(st, LOW);
    for(int i=0;i<16;i++) {
        digitalWrite(sh, HIGH); 
        digitalWrite(ds, ((waarde >> i) & 0x0001));
        digitalWrite(sh, LOW);  
    }  
    digitalWrite(sh, HIGH); 
  digitalWrite(st, HIGH);
  digitalWrite(st, LOW);
    digitalWrite(sh, LOW);  
}


int encodeMatrix(int matrix){     //In deze functie wordt de ingegeven waarde omgevormd dat de signalen op de juiste plaats staan in een nieuwe variabele die wordt gereturnd. Dit is doordat de pinnen door elkaar staan op de PCB.
  int encoded = 0x0000;

     //IC1
    encoded |= ( matrix        & 0x8000); // r1
    encoded |= ( matrix        & 0x4000); // r2
    encoded |= ((matrix << 7)  & 0x2000); // k7
    encoded |= ((matrix << 4)  & 0x1000); // r8
    encoded |= ((matrix << 7)  & 0x0800); // k5
    encoded |= ((matrix >> 3)  & 0x0400); // r3
    encoded |= ((matrix >> 2)  & 0x0200); // r5
    encoded |= ((matrix << 1)  & 0x0100); // k8
    //IC2
    encoded |= ((matrix << 4)  & 0x0080); // k4
    encoded |= ((matrix << 1)  & 0x0040); // k6
    encoded |= ((matrix << 3)  & 0x0020); // k3
    encoded |= ((matrix >> 8)  & 0x0010); // r4
    encoded |= ((matrix << 3)  & 0x0008); // k1
    encoded |= ((matrix >> 8)  & 0x0004); // r6
    encoded |= ((matrix >> 8)  & 0x0002); // r7
    encoded |= ((matrix >> 1)  & 0x0001); // k2

    return encoded;
  }
  /*
   * Bronnen:
   * Timer library en voorbeeld: https://github.com/PaulStoffregen/TimerOne
   * WriteMatrix() en encodeMatrix(): Gebasseerd op oefening van Sil Vaes.
   */
